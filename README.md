# rarn

sketch of an R package that mimics `yarn` / `npm run` scripts feature.

Put a file called `rarn.json` in your working directory and add R commands to it. For example:

```json
{
  "scripts": {
    "hello": "print('hello')",
    "test": "devtools::test()",
    "serve": "shiny::runApp('app.R')"
  }
}
```

Then, from your R console:

```r 
rarn::rarn("hello")
```

This will execute the command associated with the key "hello", i.e. it will print "hello". 